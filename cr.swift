//
//  Netguru iOS code review task
//

class paymentViewController: UIViewController {
    private (set) var delegate: PaymentViewControllerDelegate!
    private (set) var viewModel: paymentViewModel!
    private lazy var customView : PaymentView = {
        let paymentView = PaymentView()
        paymentView.didTapButton = {
            viewModel.didTapButton()
        }
        return paymentView
    }()
    
    /// initilaze paymentViewController with its viewModel and Delgate
    /// - Parameters:
    ///   - viewModel: final class  handle paymentViewController logic
    ///   - delegate: object to pass data backward
    init(_ viewModel:paymentViewModel,delegate: PaymentViewControllerDelegate) {
        self.viewModel = viewModel
        self.delegate = delegate
        super.init(nibName: "paymentViewController", bundle: .none)
    }
    
    func viewDidLoad() {
        setupViews()
        setupConstraints()
        // update UI of CustomView, after API Fetch payment Data
        viewModel.onFetchPayment = { [weak self] (currency) in
            guard let self = self else {return}
            self.CustomView.isEuro = currency == "EUR" ? true : false
            if payment!.amount != 0 {
                self.CustomView.label.text = "\(payment!.amount)"
                return
            }
        }
        
        //  pass data backward
        viewModel.onDidTapButton = { [weak self] (payment) in
            guard let self = self else {return}
            if let payment = self.payment {
                self.delegate.didFinishFlow(amount: payment.amount,
                                            currency: payment.currency)
            }
            
        }
        
        // viewModel fech payment data from API
        viewModel.fetchPayment()
    }
    
    
    /// set UI
    private func setupViews(){
        view.backgroundColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.setNavigationBarHidden(false, animated: false)
        view.addSubview(customView)
        customView.statusText = viewModel.statusTextTitle
        
    }
    
    /// set customView constaints
    private func setupConstraints(){
        customView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        customView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        
    }
    
    
}
